import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import './index.sass';
import registerServiceWorker from './registerServiceWorker';

// NOTE(Farhod): Add global configuration.
(global as any).globalConfig = {
    apiUrl: 'http://localhost/'
};

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
