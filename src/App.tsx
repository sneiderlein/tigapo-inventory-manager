import * as React from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import Store from './state/index';
import {Provider} from 'react-redux';
import axios from 'axios';
import './App.sass';

// Components
import Navigation from './components/Navigation';
import Dashboard from './components/Dashboard';
import Items from './components/Items';

class App extends React.Component {

    componentWillMount(){
        axios.get('http://localhost:3000/stuff')
            .then(res => console.log('RESPONSE: ', res.data))
            .catch(error=> console.error(error))
    }

    public render() {
        return (
            <Provider store={Store}>
                <Router>
                    <div className="app-container">
                        <div className="app-navigation-container">
                            <Navigation/>
                        </div>

                        <div className="app-main-view-container">
                            {/* Routes */}
                            <Route exact path="/" component={Dashboard}/>
                            <Route path="/items" component={Items}/>
                        </div>

                    </div>
                </Router>
            </Provider>
        );
    }
}

export default App;
