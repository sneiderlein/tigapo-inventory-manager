import * as React from 'react';
import {Link} from 'react-router-dom';
import './Navigation.sass';

class App extends React.Component {
  public render() {
    return (
        <div className="navigation-container">
            <h1 className="navigation-title">Navigation</h1>
            <div>
                <Link className="navigation-link" to="/">Dashboard</Link>
            </div>
            <div>
                <Link className="navigation-link" to="/items">Items</Link>
            </div>
        </div>
    );
  }
}

export default App;
