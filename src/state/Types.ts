export interface User {
    id: number;
    name: string;
}

export interface Location {
    id: number;
    name: string;
}

export interface Item {
    id: number;
    name: string;
    sku: string;
    quantity: number;
}
