import * as React from 'react';
import {connect} from "react-redux";
import {FullState} from "../state";
import {User, Location} from "../state/Types";
import {getCurrentUser, getCurrentLocation} from "../state/actions/MainActions";
import './Dashboard.sass';

const MapState = (fullState: FullState) => {
    let {user, location, isFetchingUser, isFetchingLocation} = fullState.Main;
    return { user, location };
};

export interface DashboardProps {
    user: User | null,
    location: Location | null,
    isFetchingUser: boolean,
    isFetchingLocation: boolean,

    // actions
    getCurrentUser: () => void,
    getCurrentLocation: () => void,
}

class Dashboard extends React.Component<DashboardProps> {

    componentWillMount(){
        this.props.getCurrentUser();
        this.props.getCurrentLocation();
    }

    public render() {
        return (
            <div className="dashboard-container">
                <h1 className="dashboard-title">Dashboard</h1>
                {this.props.user && this.props.location
                    ? <div>
                        <div>Hello, <strong>{this.props.user.name}</strong></div>
                        <div>Location <strong>{this.props.location.name}</strong></div>
                    </div>
                    : <p>Loading...</p>
                }
            </div>
        );
    }
}

export default connect(MapState, {getCurrentUser, getCurrentLocation})(Dashboard);
