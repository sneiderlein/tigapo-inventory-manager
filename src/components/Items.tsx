import * as React from 'react';
import {connect} from "react-redux";
import {FullState} from "../state";
import './Items.sass'
import {Item} from "../state/Types";
import {getItems} from "../state/actions/MainActions";
import * as _ from 'lodash';

const MapState = (fullState: FullState) => {
    const {items, isFetchingItems, itemsFetchError} = fullState.Main;
    return {items, isFetchingItems, itemsFetchError};
};

export interface ItemsProps {
    // Items Data
    items: Item[],
    isFetchingItems: boolean,
    itemsFetchError: string,

    // Actions
    getItems: () => void,
}

class Items extends React.Component<ItemsProps> {
    componentWillMount(){
        this.props.getItems();
    }

    renderItems = () => {
        if(!this.props.items){
            return (<div>loading...</div>);
        }
        return (
            <div>
                {_.map(this.props.items, item => {
                    return (
                        <div className="items-single-item-container">
                            {_.map(item, (val, key) => {
                                return <div className="items-single-item-row"><strong>{key}</strong>: {val}</div>
                            })}
                        </div>
                    )
                })}
            </div>
        );
    };

    public render() {
        return (
            <div className="items-container">
                <h1 className="items-title">Items</h1>
                {this.renderItems()}
            </div>
        );
    }
}

export default connect(MapState, {getItems})(Items);
