import {createAction} from 'redux-actions';
import axios from 'axios';
import {FullState} from "../index";
import {Item, User} from "../Types";
import config from '../../config';

// Action Creators
// User
export const getCurrentUserAttempt = createAction('@Main/get_current_user_attempt');
export const getCurrentUserSuccess = createAction('@Main/get_current_user_success', (user: User) => ({user}));
export const getCurrentUserFailed  = createAction('@Main/get_current_user_failed', (error: string) => ({error}));

// TODO(Farhod): Maybe we should abstract the API out to a "services" file?
export const getCurrentUser = () => {
    return (dispatch: Function, getState: () => FullState) => {
        dispatch(getCurrentUserAttempt());
        axios.get(config.apiUrl + '/users/current')
            .then(response => {
                console.log('Got the response for user: ', response);
                dispatch(getCurrentUserSuccess(response.data));
            })
            .catch(error => {
                // TODO(Farhod): think of an error handling mechanism.
                console.log('Got the response ERROR: ', error);
                dispatch(getCurrentUserFailed(error.toString()));
            });
    }
};

// Location
export const getCurrentLocationAttempt = createAction('@Main/get_current_location_attempt');
export const getCurrentLocationSuccess = createAction('@Main/get_current_location_success', (location: Location) => ({location}));
export const getCurrentLocationFailed  = createAction('@Main/get_current_location_failed', (error: string) => ({error}));

export const getCurrentLocation = () => {
    return (dispatch: Function, getState: () => FullState) => {
        dispatch(getCurrentLocationAttempt());
        axios.get(config.apiUrl + '/locations/current')
            .then(response => {
                console.log('Got the response for location: ', response);
                dispatch(getCurrentLocationSuccess(response.data));
            })
            .catch(error => {
                // TODO(Farhod): think of an error handling mechanism.
                console.log('Got the response ERROR: ', error);
                dispatch(getCurrentLocationFailed(error.toString()));
            });
    }
};

// NOTE(Farhod): This might not be the right place for these actions. It may be that if the "items" are a big part of the
// app, they might need their own reducer and actions file. But for now, I'll leave them here.

// Items

export const getItemsAttempt = createAction('@Main/get_items_attempt');
export const getItemsSuccess = createAction('@Main/get_items_success', (items: Item[]) => ({items}));
export const getItemsFailed  = createAction('@Main/get_items_failed', (error: string) => ({error}));


export const getItems = () => {
    return (dispatch: Function, getState: () => FullState) => {
        dispatch(getItemsAttempt());
        axios.get(config.apiUrl + '/items/')
            .then(response => {
                console.log('Got the response for location: ', response);
                dispatch(getItemsSuccess(response.data));
            })
            .catch(error => {
                // TODO(Farhod): think of an error handling mechanism.
                console.log('Got the response ERROR: ', error);
                dispatch(getItemsFailed(error.toString()));
            });
    }
};
