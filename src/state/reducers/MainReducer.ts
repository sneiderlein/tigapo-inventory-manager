import {User, Location, Item} from "../Types";
const {handleActions} = require('redux-actions');
import * as MainActions from "../actions/MainActions";

export interface MainReducerState  {
    // User
    user: User | null;
    userFetchError: string,
    isFetchingUser: boolean,

    // Location
    location: Location | null;
    locationFetchError: string,
    isFetchingLocation: boolean,

    // Items
    items: Item[],
    isFetchingItems: boolean,
    itemsFetchError: string,
}

const INITIAL_STATE: MainReducerState = {
    // User
    user: null,
    userFetchError: '',
    isFetchingUser: false,

    // Location
    location: null,
    locationFetchError: '',
    isFetchingLocation: false,

    // Items
    items: [],
    isFetchingItems: false,
    itemsFetchError: '',
};

export const MainReducer = handleActions({

    // User
    // NOTE(Farhod): "as any" seems to make TS happy. Should investigate.
    [MainActions.getCurrentUserAttempt as any]: (state: MainReducerState): MainReducerState => {
        return {...state, user: null, userFetchError: '', isFetchingUser: true};
    },
    [MainActions.getCurrentUserSuccess as any]: (state: MainReducerState, {payload: {user}}): MainReducerState => {
        return {...state, user, userFetchError: '', isFetchingUser: false};
    },
    [MainActions.getCurrentUserFailed as any]: (state: MainReducerState, {payload: {error}}): MainReducerState => {
        return {...state, user: null, userFetchError: error, isFetchingUser: false};
    },


    // Location
    [MainActions.getCurrentLocationAttempt as any]: (state: MainReducerState): MainReducerState => {
        return {...state, location: null, locationFetchError: '', isFetchingLocation: true};
    },
    [MainActions.getCurrentLocationSuccess as any]: (state: MainReducerState, {payload: {location}}): MainReducerState => {
        return {...state, location, locationFetchError: '', isFetchingLocation: false};
    },
    [MainActions.getCurrentLocationFailed as any]: (state: MainReducerState, {payload: {error}}): MainReducerState => {
        return {...state, location: null, locationFetchError: error, isFetchingLocation: false};
    },

    // Location
    [MainActions.getItemsAttempt as any]: (state: MainReducerState): MainReducerState => {
        return {...state, items: [], itemsFetchError: '', isFetchingItems: true};
    },
    [MainActions.getItemsSuccess as any]: (state: MainReducerState, {payload: {items}}): MainReducerState => {
        return {...state, items, itemsFetchError: '', isFetchingItems: false};
    },
    [MainActions.getItemsFailed as any]: (state: MainReducerState, {payload: {error}}): MainReducerState => {
        return {...state, location: null, itemsFetchError: error, isFetchingItems: false};
    },

}, INITIAL_STATE);