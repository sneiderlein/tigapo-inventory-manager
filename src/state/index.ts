import {createStore, applyMiddleware, combineReducers} from "redux";
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import {MainReducer, MainReducerState} from "./reducers/MainReducer";

export interface FullState {
    Main: MainReducerState
}

const reducers = {
    Main: MainReducer,
};

// TODO(Farhod): remove the logger component on production.
export default createStore(
    combineReducers(reducers),
    applyMiddleware(thunk, logger),
);
